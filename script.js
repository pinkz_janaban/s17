console.log("**JS ARRAYS**");
/*
Arrays - used to store multiple related values in a single variable
-declared using square brackets [] known as array literals

Elements - are values inside an array

index - 0 = offset
element 1 = index 0

syntax: let/const arrayName = [elementA, elementB, elementC]


*/

let grades = [98, 94, 89, 90];
let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];

let mixedArr = [12, 'Asus', null, undefined, {}]; //not recommended
console.log(mixedArr);

//Reading
console.log(grades[0]);
console.log(computerBrands[3]);
console.log(grades[10]); //this will result as undefined

let myTasks = [
	'bake sass',
	'drink html',
	'inhale css',
	'eat javascript',
];

console.log(myTasks);

//Reassigning value

myTasks[0] = 'hello world';
console.log(myTasks);

//Getting the lenght of an array
console.log(computerBrands.length);

let lastIndex = myTasks.length - 1;
console.log(lastIndex);


/*ARRAY METHODS
1. Mutator Methods - are functions that 'mutate' or change an array
push - adds an element in the end of the array and return its length
	syntax: arrayName.push(elementA, elementB)
pop - removes the last element in an array and return the removed element
shift - removes an element at the beginning of an array and return the removed element
unshift - adds one or more element/s at the beginning of an array
splice - removes element from specified index and add new elements
	syntax: arrayName.splice(startingIndex, deleteCount, elements to be added)
sort - rearranges the elements in alphanumeric order
reverse - reverses the order of an array

2. Non Mutator Methods
- are functions that do not modify or change the array

indexOf() - returns the first index of the first matching element found in an arrray
- if no match is found, it returns -1

last IndexOf() - returns the last matching element found in an array 
	syntax: arrayName.lastIndexOf(searchValue);
	arrayName.lastIndexOf(searchValue, fromIndex);

slice - slices a portion of an array and return a new array
	syntax: arrayName.slice(startingIndex);
	syntax: arrayName.slice(startingIndex, endingIndex);

toString - 

concat - combines two or more arrays and return the combined result


join - returns an array as string separated by specified separator

3. Iteration Methods
- loops design to perform repetitive tasks on arrays

foreach
	syntax: arrayName.forEach(function(individualElement){
		statement
	})

map - iterates on each element and returns new array with a different values depending on the results of the function's operation

every

filter - returns a new array that meets the given condition

includes

reduce 
	let/const resultArray = arrayName.reduce(function(accumulatro, currentValue){
		statement
	})


*/

//Mutator Methods: push
let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];
console.log('Current array');
console.log(fruits);
console.log('Array after push()');
let fruitsLenght = fruits.push('Mongo');
console.log(fruits);
console.log(fruitsLenght);

let addfruitsLength = fruits.push("Peach");
console.log(addfruitsLength);


//Mutator Methods: pop
let removedFruit = fruits.pop();
console.log('Mutated array after pop()');
console.log(fruits);
console.log(removedFruit);


//Mutator Methods: shift
let firstFruit = fruits.shift();
console.log('Mutated array after shift()');
console.log(fruits);
console.log(firstFruit);

//Mutator Methods: unshift
fruits.unshift('Lime', 'Banana');
console.log('Mutated array after unshift()');
console.log(fruits);

//Mutator Methods: splice
fruits.splice(1);
fruits.splice(2, 2);
fruits.splice(1, 2, 'Cherry', 'Grapes');
console.log('Mutated array after splice()');
console.log(fruits);

//Mutator Methods: sort
fruits.sort();
console.log('Mutated array after sort()');
console.log(fruits);

//Mutator Methods: reverse
fruits.reverse();
console.log('Mutated array after reverse()');
console.log(fruits);

//Non Mutator Methods: indexOf()
let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];
let firstIndex = countries.indexOf('PH');
console.log('Result of indexOf() method: ' + firstIndex);

let invalidCountry = countries.indexOf('BR');
console.log('Result of indexOf() method: ' + invalidCountry);

//Non Mutator Methods: lastIndexOf()
let lastIndex_NM = countries.lastIndexOf('PH');
console.log('Result of lastIndexOf() method: ' + lastIndex_NM);

let lastIndexStart_NM = countries.indexOf('PH', 2);
console.log('Result of lastIndexOf() method: ' + lastIndexStart_NM);


//Non Mutator Methods: slice
let sliceArrayA = countries.slice(2);
console.log('Result of slice method A');
console.log(sliceArrayA);

let sliceArrayB = countries.slice(2, 4);
console.log('Result of slice method B');
console.log(sliceArrayB);

let sliceArrayC = countries.slice(-3);
console.log('Result of slice method C');
console.log(sliceArrayC);


//Non Mutator Methods: slice
let stringArray = countries.toString();
console.log('Result from toStrong');
console.log(stringArray);

//Non Mutator Methods: concat
let tasksArrayA = ['drink HTML', 'eat javascript'];
let tasksArrayB = ['inhale CSS', 'breathe sass'];
let tasksArrayC = ['get git', 'be node'];

let tasks = tasksArrayA.concat(tasksArrayB);
console.log('Result from concat()');
console.log(tasks);

let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
console.log('Result from concat()');
console.log(allTasks);

let combinedTasks = tasksArrayA.concat('smell express', 'throw react');
console.log('Result from concat()');
console.log(combinedTasks);


//Non Mutator Methods: join
let users = ['John', 'Jane', 'Joe', 'Robert'];
console.log(users.join());
console.log(users.join(''));
console.log(users.join(' - '));
console.log(users.join(' & '));


//Iteration Methods: foreach
countries.forEach(function(country){
	console.log(country);
})


allTasks.forEach(function(task){
	console.log(task);
})

let filteredTasks = [];

allTasks.forEach(function(task){
	if(task.length > 10){
		filteredTasks.push(task);
	}
})
console.log('Result from forEach()');
console.log(filteredTasks);


//Iteration Methods: map
let numbers = [1, 2, 3, 4, 5];

let numberMap = numbers.map(function(number){
	return number*number;
})
console.log('Result from map()');
console.log(numberMap);


//example of applying foreach
let numberMap2 = [];
numbers.forEach(function(number){
	let square = number * number;
	numberMap2.push(square);
})
console.log('Result from forEach()');
console.log(numberMap2);


//example of applying map for strings array 
let arrayMap = allTasks.map(function(task){
	return 'I ' + tasks;
})

console.log(arrayMap);


//Iteration Methods: every

let allValid = numbers.every(function(number){
	return (number < 3);
})
console.log('Result from every()');
console.log(allValid);

//Iteration Methods: some

let someValid = numbers.some(function(number){
	return (number < 3);
})
console.log('Result from some()');
console.log(someValid);

//Iteration Methods: filter
let filterValid = numbers.filter(function(number){
	return (number < 3);
})
console.log('Result from filter()');
console.log(filterValid);


//Iteration Methods: includes
let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

let filterProducts = products.filter(function(product){
	return product.toLowerCase().includes('a');
})
console.log('Result from filter() & includes');
console.log(filterProducts);

//Iteration Methods: Multi-dimensional Array
let chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];
console.log(chessBoard);
console.log(chessBoard[1][4]); //e2
console.log('Pawn moves to f2: ' + chessBoard[1][5]);
console.log('Knight moves to c7: ' + chessBoard[6][2]);


let printStudents = ["Abby","Bryce", "Chase"]

function printStudents1() {
    // Your code here!
    printStudents1.sort()
}
console.log(printStudents1.sort)